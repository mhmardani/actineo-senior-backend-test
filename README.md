README
===

## Instructions

1. Checkout the project
2. Execute `composer install`
3. To use the project execute `php src/index.php`

## Tests

```php
php vendor/bin/phpunit
```
