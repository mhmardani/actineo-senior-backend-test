<?php

use Actineos\PhpCliRaceGameTest\Library\RaceCalculator;
use Actineos\PhpCliRaceGameTest\Object\Vehicle;
use PHPUnit\Framework\TestCase;

final class RaceCalculatorTest extends TestCase
{
    // Happy paths
    public function testShouldCalculateTimeToFinish() {
        $speed = 100;
        $distance = 100;
        $vehicle = new Vehicle();
        $vehicle->setSpeed($speed, 'kmh');
        $calculator = new RaceCalculator($vehicle, $distance);
        $this->assertEquals($calculator->getTimeToFinish(), ($distance/ ($speed * 5 / 18)));
    }

    // Edge cases
    public function testShouldGetException() {
        try {
            $speed = 100;
            $distance = 0;
            $vehicle = new Vehicle();
            $vehicle->setSpeed($speed, 'kmh');
            new RaceCalculator($vehicle, $distance);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'Invalid distance');
        }
    }
}
