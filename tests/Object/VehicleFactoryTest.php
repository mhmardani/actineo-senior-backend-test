<?php

use Actineos\PhpCliRaceGameTest\Object\Vehicle;
use Actineos\PhpCliRaceGameTest\Object\VehicleFactory;
use PHPUnit\Framework\TestCase;

final class VehicleFactoryTest extends TestCase
{
    // Happy paths
    public function testShouldCreateNewVehicle() {
        $filename = 'car';
        $jsonObject = new stdClass();
        $jsonObject->speed = new stdClass();
        $jsonObject->speed->unit = 'kmh';
        $jsonObject->speed->value = 100;
        $vehicle = (new VehicleFactory($filename, $jsonObject))->create();
        $this->assertInstanceOf(Vehicle::class, $vehicle);
    }
}
