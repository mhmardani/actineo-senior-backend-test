<?php

use Actineos\PhpCliRaceGameTest\Object\Vehicle;
use PHPUnit\Framework\TestCase;

final class VehicleTest extends TestCase
{
    public function testShouldGetSpeedInMetersPerSecond() {
        $vehicle = new Vehicle();
        $vehicle->setSpeed(100, 'kmh');
        $this->assertEquals($vehicle->getSpeed()->toMetersPerSecond(), (100 * 5) / 18);
    }
}
