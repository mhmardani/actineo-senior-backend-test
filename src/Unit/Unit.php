<?php

namespace Actineos\PhpCliRaceGameTest\Unit;

abstract class Unit implements IUnit
{
    protected $value;

    public function __construct(float $value) {
        $this->value = $value;
    }
}
