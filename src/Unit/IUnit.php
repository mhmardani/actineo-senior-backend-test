<?php

namespace Actineos\PhpCliRaceGameTest\Unit;

interface IUnit
{
    public function toMetersPerSecond(): float;
}
