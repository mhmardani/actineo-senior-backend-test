<?php

namespace Actineos\PhpCliRaceGameTest\Unit;

class KmhUnit extends Unit
{
    public function toMetersPerSecond(): float {
        return $this->value * 5 / 18;
    }
}
