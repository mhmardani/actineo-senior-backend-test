<?php

namespace Actineos\PhpCliRaceGameTest\Unit;

class MphUnit extends Unit
{
    public function toMetersPerSecond(): float {
        return $this->value * 0.44704;
    }
}
