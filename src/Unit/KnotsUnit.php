<?php

namespace Actineos\PhpCliRaceGameTest\Unit;

class KnotsUnit extends Unit
{
    public function toMetersPerSecond(): float {
        return $this->value * 0.514444;
    }
}
