<?php

namespace Actineos\PhpCliRaceGameTest;

require_once dirname(__DIR__) . '/vendor/autoload.php';

use Actineos\PhpCliRaceGameTest\Library\RaceCalculator;
use Actineos\PhpCliRaceGameTest\Object\VehicleFactory;
use cli\Notify;
use cli\notify\Dots;
use cli\Table;
use cli\table\Ascii;
use Exception;
use function cli\line;
use function cli\menu;
use function cli\prompt;

if (php_sapi_name() != 'cli') die('Must run from command line');

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
ini_set('log_errors', 0);
ini_set('html_errors', 0);

define("VEHICLE_PATH", dirname(__DIR__) . '/data/vehicles');

displayHeader();
$numberOfParticipants = requestParticipants();
$pickedVehicles = requestVehicles($numberOfParticipants);
$raceDistance = requestRaceDistance();

test_notify(new Dots('Loading', 5, 0), 10, 300000);

$data = [];
foreach($pickedVehicles as $pickedVehicle){
    $vehicleRaceCalculator = new RaceCalculator($pickedVehicle, $raceDistance);

    $totalTimeToFinish = $vehicleRaceCalculator->getTimeToFinish();

    array_push($data, [
        'vehicleName' => $pickedVehicle->getName(),
        'totalTimeToFinish' => $totalTimeToFinish
    ]);
}

usort($data, function ($param1, $param2) {
    return $param1['totalTimeToFinish'] >= $param2['totalTimeToFinish'] ? 1 : -1;
});

$table = new Table();
$table->setHeaders(['Name', 'Time']);
$table->setRows($data);
$table->setRenderer(new Ascii([40, 40]));
$table->setFooters(['The Winner is: '.$data[0]['vehicleName']]);
$table->display();

function displayHeader(): void {
    line('CLI Race Game');
    line('===');
}

function requestParticipants(): string {
    return prompt('How many vehicles will participate in the race?', 0, ' ');
}

function requestVehicles(int $numberOfParticipants): array {
    $availableVehicles = getVehicleOptions(VEHICLE_PATH);
    $vehicles = [];
    for ($i=0; $i < $numberOfParticipants; $i++){
        $index = menu(array_map('Actineos\PhpCliRaceGameTest\_removeExtension', $availableVehicles), null, 'Pick a vehicle');
        $vehicles[$index] = createVehicle($availableVehicles[$index]);
    }
    return $vehicles;
}

function _removeExtension(string $value): string {
    return pathinfo($value, PATHINFO_FILENAME);
}

function getVehicleOptions(string $path): array {
    return array_diff(scandir($path), array('..', '.'));
}

function requestRaceDistance(string $msg = 'What distance in meters does cover the race?') {
    $value = prompt($msg, 0, ' ');
    if (!is_numeric($value)){
        return requestRaceDistance('The distance must be an integer. Again, what distance does cover the race?');
    }

    return (int) $value;
}

function createVehicle(string $filename) {
    $filepath = VEHICLE_PATH.'/'.$filename;
    if (!file_exists($filepath)){
        throw new Exception("Vehicle object not found");
    }

    $jsonObject = json_decode(file_get_contents($filepath));
    if (!$jsonObject){
        throw new Exception("Invalid vehicle object");
    }

    return (new VehicleFactory(_removeExtension($filename), $jsonObject))->create();
}

function test_notify(Notify $notify, $cycle = 1000000, $sleep = null) {
    for ($i = 0; $i < $cycle; $i++){
        $notify->tick();
        if ($sleep){
            usleep($sleep);
        }
    }
    $notify->finish();
}
