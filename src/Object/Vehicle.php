<?php

namespace Actineos\PhpCliRaceGameTest\Object;

use Actineos\PhpCliRaceGameTest\Unit\IUnit;
use Actineos\PhpCliRaceGameTest\Object\Exception\UnknownUnitException;

class Vehicle implements IVehicle
{
    private $name;

    private $speedValue;

    private $speedUnit;

    public function setName(string $value): void {
        $this->name = $value;
    }

    public function setSpeed(float $value, string $unit): void {
        $this->speedValue = $value;
        $this->speedUnit = $unit;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getSpeed(): IUnit {
        $unitClassName = $this->getUnitClassName();
        return (new $unitClassName($this->speedValue));
    }

    private function getUnitClassName(): string {
        $unitClassName = 'Actineos\PhpCliRaceGameTest\Unit\\' . ucfirst($this->speedUnit) . 'Unit';
        if (!class_exists($unitClassName)) {
            throw new UnknownUnitException($unitClassName);
        }
        return $unitClassName;
    }
}
