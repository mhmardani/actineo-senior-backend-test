<?php

// This class is not necessary, exists to check YAGNI principle

namespace Actineos\PhpCliRaceGameTest\Object;

class VehicleFactory
{
    private $json;
    private $name;

    public function __construct(string $name, object $json) {
        $this->json = $json;
        $this->name = $name;
    }

    public function create(): IVehicle {
        $vehicle = new Vehicle();
        $vehicle->setName($this->name);
        $vehicle->setSpeed($this->json->speed->value, $this->json->speed->unit);

        return $vehicle;
    }
}
