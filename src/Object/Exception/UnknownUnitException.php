<?php

namespace Actineos\PhpCliRaceGameTest\Object\Exception;

use Exception;
use Throwable;

class UnknownUnitException extends Exception
{
    public function __construct($unitName, $code = 0, Throwable $previous = null) {
        parent::__construct("Unknown unit: " . $unitName, $code, $previous);
    }
}
