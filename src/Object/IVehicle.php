<?php

namespace Actineos\PhpCliRaceGameTest\Object;

use Actineos\PhpCliRaceGameTest\Unit\IUnit;

interface IVehicle
{
    public function setName(string $value): void;

    public function setSpeed(float $value, string $unit): void;

    public function getName(): string;

    public function getSpeed(): IUnit;
}
