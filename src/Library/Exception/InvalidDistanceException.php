<?php

namespace Actineos\PhpCliRaceGameTest\Library\Exception;

use Exception;

class InvalidDistanceException extends Exception
{
    protected $message = "Invalid distance";
}
