<?php

namespace Actineos\PhpCliRaceGameTest\Library;

use Actineos\PhpCliRaceGameTest\Object\IVehicle;
use Actineos\PhpCliRaceGameTest\Library\Exception\InvalidDistanceException;

class RaceCalculator
{
    private $distance;

    private $vehicle;

    public function __construct(IVehicle $vehicle, int $distance) {
        $this->vehicle = $vehicle;
        $this->distance = $distance;

        if ($this->distance <= 0) {
            throw new InvalidDistanceException();
        }
    }

    public function getTimeToFinish(): float | int {
        return $this->distance / $this->vehicle->getSpeed()->toMetersPerSecond();
    }
}
